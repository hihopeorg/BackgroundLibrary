package com.noober.background.drawable;

import ohos.agp.components.element.Element;

public interface ICreateDrawable {
    Element create() throws Exception;
}
