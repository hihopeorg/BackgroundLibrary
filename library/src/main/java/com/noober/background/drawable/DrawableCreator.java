package com.noober.background.drawable;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;


public class DrawableCreator {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000200, "DrawableCreator");


    public enum Shape {

        Rectangle(0), Oval(1), Line(2), Ring(3);

        int value;

        Shape(int value) {
            this.value = value;
        }
    }

    public enum Gradient {

        Linear(0), Radial(1), Sweep(2);

        int value;

        Gradient(int value) {
            this.value = value;
        }
    }

    public enum DrawablePosition {

        Left, Right, Top, Bottom

    }

    public static class Builder {
        private Shape shape = Shape.Rectangle;
        private Integer solidColor;
        private Float cornersRadius;
        private Float cornersBottomLeftRadius;
        private Float cornersBottomRightRadius;
        private Float cornersTopLeftRadius;
        private Float cornersTopRightRadius;

        //默认渐变角度
        private int gradientAngle = -1;
        private Float gradientCenterX;
        private Float gradientCenterY;
        private Integer gradientCenterColor;
        private Integer gradientEndColor;
        private Integer gradientStartColor;
        private Float gradientRadius;
        private Gradient gradient = Gradient.Linear;
        private boolean useLevel = false;

        private Rect padding = new Rect();

        private Float sizeWidth;
        private Float sizeHeight;
        private Integer strokeWidth;
        private Integer strokeColor;
        private float strokeDashWidth = 0;
        private float strokeDashGap = 0;
        private boolean rippleEnable = false;
        private Integer rippleColor;

        private Integer checkableStrokeColor;
        private Integer checkedStrokeColor;
        private Integer enabledStrokeColor;
        private Integer selectedStrokeColor;
        private Integer pressedStrokeColor;
        private Integer focusedStrokeColor;
        private Integer unCheckableStrokeColor;
        private Integer unCheckedStrokeColor;
        private Integer unEnabledStrokeColor;
        private Integer unSelectedStrokeColor;
        private Integer unPressedStrokeColor;
        private Integer unFocusedStrokeColor;

        private Integer checkableSolidColor;
        private Integer checkedSolidColor;
        private Integer enabledSolidColor;
        private Integer selectedSolidColor;
        private Integer pressedSolidColor;
        private Integer focusedSolidColor;
        private Integer unCheckableSolidColor;
        private Integer unCheckedSolidColor;
        private Integer unEnabledSolidColor;
        private Integer unSelectedSolidColor;
        private Integer unPressedSolidColor;
        private Integer unFocusedSolidColor;

        private Element checkableDrawable;
        private Element checkedDrawable;
        private Element enabledDrawable;
        private Element selectedDrawable;
        private Element pressedDrawable;
        private Element focusedDrawable;
        private Element focusedHovered;
        private Element focusedActivated;

        private Element unCheckableDrawable;
        private Element unCheckedDrawable;
        private Element unEnabledDrawable;
        private Element unSelectedDrawable;
        private Element unPressedDrawable;
        private Element unFocusedDrawable;
        private Element unFocusedHovered;
        private Element unFocusedActivated;

        private Integer checkableTextColor;
        private Integer checkedTextColor;
        private Integer enabledTextColor;
        private Integer selectedTextColor;
        private Integer pressedTextColor;
        private Integer focusedTextColor;

        private Integer unCheckableTextColor;
        private Integer unCheckedTextColor;
        private Integer unEnabledTextColor;
        private Integer unSelectedTextColor;
        private Integer unPressedTextColor;
        private Integer unFocusedTextColor;
        private int textColorCount;
        private int mClickCount;

        private boolean hasSelectDrawable = false;

        private ShapeElement baseGradientDrawable = null;

        private StateElement baseStateListDrawable = null;

        public Builder setShape(Shape shape) {
            this.shape = shape;
            return this;
        }

        public Builder setSolidColor(int solidColor) {
            this.solidColor = solidColor;
            return this;
        }

        public Builder setCornersRadius(float cornersRadius) {
            this.cornersRadius = cornersRadius;
            return this;
        }

        public Builder setCornersRadius(float cornersBottomLeftRadius, float cornersBottomRightRadius, float cornersTopLeftRadius, float cornersTopRightRadius) {
            this.cornersBottomLeftRadius = cornersBottomLeftRadius;
            this.cornersBottomRightRadius = cornersBottomRightRadius;
            this.cornersTopLeftRadius = cornersTopLeftRadius;
            this.cornersTopRightRadius = cornersTopRightRadius;
            return this;
        }


        public Builder setGradientAngle(int gradientAngle) {
            this.gradientAngle = gradientAngle;
            return this;
        }

        public Builder setGradientCenterXY(float gradientCenterX, float gradientCenterY) {
            this.gradientCenterX = gradientCenterX;
            this.gradientCenterY = gradientCenterY;
            return this;
        }


        public Builder setGradientColor(int startColor, int endColor) {
            this.gradientStartColor = startColor;
            this.gradientEndColor = endColor;
            return this;
        }


        public Builder setGradientColor(int startColor, int centerColor, int endColor) {
            this.gradientStartColor = startColor;
            this.gradientCenterColor = centerColor;
            this.gradientEndColor = endColor;
            return this;
        }

        public Builder setGradientRadius(float gradientRadius) {
            this.gradientRadius = gradientRadius;
            return this;
        }

        public Builder setGradient(Gradient gradient) {
            this.gradient = gradient;
            return this;
        }

        public Builder setUseLevel(boolean useLevel) {
            this.useLevel = useLevel;
            return this;
        }

        public Builder setPadding(float paddingLeft, float paddingTop, float paddingRight, float paddingBottom) {
            padding.left = (int) paddingLeft;
            padding.top = (int) paddingTop;
            padding.right = (int) paddingRight;
            padding.bottom = (int) paddingBottom;
            return this;
        }

        public Builder setSizeWidth(float sizeWidth) {
            this.sizeWidth = sizeWidth;
            return this;
        }

        public Builder setSizeHeight(float sizeHeight) {
            this.sizeHeight = sizeHeight;
            return this;
        }

        public Builder setStrokeWidth(int strokeWidth) {
            this.strokeWidth = strokeWidth;
            return this;
        }

        public Builder setStrokeColor(int strokeColor) {
            this.strokeColor = strokeColor;
            return this;
        }

        public Builder setStrokeDashWidth(float strokeDashWidth) {
            this.strokeDashWidth = strokeDashWidth;
            return this;
        }

        public Builder setStrokeDashGap(float strokeDashGap) {
            this.strokeDashGap = strokeDashGap;
            return this;
        }

        public Builder setRipple(boolean rippleEnable, int rippleColor) {
            this.rippleEnable = rippleEnable;
            this.rippleColor = rippleColor;
            return this;
        }

        public Builder setCheckableStrokeColor(int checkableStrokeColor, int unCheckableStrokeColor) {
            this.checkableStrokeColor = checkableStrokeColor;
            this.unCheckableStrokeColor = unCheckableStrokeColor;
            return this;
        }

        public Builder setCheckedStrokeColor(int checkedStrokeColor, int unCheckedStrokeColor) {
            this.checkedStrokeColor = checkedStrokeColor;
            this.unCheckedStrokeColor = unCheckedStrokeColor;
            return this;
        }

        public Builder setEnabledStrokeColor(int enabledStrokeColor, int unEnabledStrokeColor) {
            this.enabledStrokeColor = enabledStrokeColor;
            this.unEnabledStrokeColor = unEnabledStrokeColor;
            return this;
        }

        public Builder setSelectedStrokeColor(int selectedStrokeColor, int unSelectedStrokeColor) {
            this.selectedStrokeColor = selectedStrokeColor;
            this.unSelectedStrokeColor = unSelectedStrokeColor;
            return this;
        }

        public Builder setPressedStrokeColor(int pressedStrokeColor, int unPressedStrokeColor) {
            this.pressedStrokeColor = pressedStrokeColor;
            this.unPressedStrokeColor = unPressedStrokeColor;
            return this;
        }

        public Builder setFocusedStrokeColor(int focusedStrokeColor, int unFocusedStrokeColor) {
            this.focusedStrokeColor = focusedStrokeColor;
            this.unFocusedStrokeColor = unFocusedStrokeColor;
            return this;
        }

        public Builder setCheckableSolidColor(int checkableSolidColor, int unCheckableSolidColor) {
            this.checkableSolidColor = checkableSolidColor;
            this.unCheckableSolidColor = unCheckableSolidColor;
            return this;
        }

        public Builder setCheckedSolidColor(int checkedSolidColor, int unCheckedSolidColor) {
            this.checkedSolidColor = checkedSolidColor;
            this.unCheckedSolidColor = unCheckedSolidColor;
            return this;
        }

        public Builder setEnabledSolidColor(int enabledSolidColor, int unEnabledSolidColor) {
            this.enabledSolidColor = enabledSolidColor;
            this.unEnabledSolidColor = unEnabledSolidColor;
            return this;
        }

        public Builder setSelectedSolidColor(int selectedSolidColor, int unSelectedSolidColor) {
            this.selectedSolidColor = selectedSolidColor;
            this.unSelectedSolidColor = unSelectedSolidColor;
            return this;
        }

        public Builder setPressedSolidColor(int pressedSolidColor, int unPressedSolidColor) {
            this.pressedSolidColor = pressedSolidColor;
            this.unPressedSolidColor = unPressedSolidColor;
            return this;
        }

        public Builder setFocusedSolidColor(int focusedSolidColor, int unFocusedSolidColor) {
            this.focusedSolidColor = focusedSolidColor;
            this.unFocusedSolidColor = unFocusedSolidColor;
            return this;
        }

        public Builder setCheckableDrawable(Element checkableDrawable) {
            this.hasSelectDrawable = true;
            this.checkableDrawable = checkableDrawable;
            return this;
        }

        public Builder setCheckedDrawable(Element checkedDrawable) {
            this.hasSelectDrawable = true;
            this.checkedDrawable = checkedDrawable;
            return this;
        }

        public Builder setEnabledDrawable(Element enabledDrawable) {
            this.hasSelectDrawable = true;
            this.enabledDrawable = enabledDrawable;
            return this;
        }

        public Builder setSelectedDrawable(Element selectedDrawable) {
            this.hasSelectDrawable = true;
            this.selectedDrawable = selectedDrawable;
            return this;
        }

        public Builder setPressedDrawable(Element pressedDrawable) {
            this.hasSelectDrawable = true;
            this.pressedDrawable = pressedDrawable;
            return this;
        }

        public Builder setFocusedDrawable(Element focusedDrawable) {
            this.hasSelectDrawable = true;
            this.focusedDrawable = focusedDrawable;
            return this;
        }

        public Builder setFocusedHovered(Element focusedHovered) {
            this.hasSelectDrawable = true;
            this.focusedHovered = focusedHovered;
            return this;
        }

        public Builder setFocusedActivated(Element focusedActivated) {
            this.hasSelectDrawable = true;
            this.focusedActivated = focusedActivated;
            return this;
        }

        public Builder setUnCheckableDrawable(Element unCheckableDrawable) {
            this.hasSelectDrawable = true;
            this.unCheckableDrawable = unCheckableDrawable;
            return this;
        }

        public Builder setUnCheckedDrawable(Element unCheckedDrawable) {
            this.hasSelectDrawable = true;
            this.unCheckedDrawable = unCheckedDrawable;
            return this;
        }

        public Builder setUnEnabledDrawable(Element unEnabledDrawable) {
            this.hasSelectDrawable = true;
            this.unEnabledDrawable = unEnabledDrawable;
            return this;
        }

        public Builder setUnSelectedDrawable(Element unSelectedDrawable) {
            this.hasSelectDrawable = true;
            this.unSelectedDrawable = unSelectedDrawable;
            return this;
        }

        public Builder setUnPressedDrawable(Element unPressedDrawable) {
            this.hasSelectDrawable = true;
            this.unPressedDrawable = unPressedDrawable;
            return this;
        }

        public Builder setUnFocusedDrawable(Element unFocusedDrawable) {
            this.hasSelectDrawable = true;
            this.hasSelectDrawable = true;
            this.unFocusedDrawable = unFocusedDrawable;
            return this;
        }

        public Builder setUnFocusedHovered(Element unFocusedHovered) {
            this.hasSelectDrawable = true;
            this.unFocusedHovered = unFocusedHovered;
            return this;
        }

        public Builder setUnFocusedActivated(Element unFocusedActivated) {
            this.hasSelectDrawable = true;
            this.unFocusedActivated = unFocusedActivated;
            return this;
        }

        public Builder setCheckableTextColor(int checkableTextColor) {
            this.checkableTextColor = checkableTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setCheckedTextColor(int checkedTextColor) {
            this.checkedTextColor = checkedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setEnabledTextColor(int enabledTextColor) {
            this.enabledTextColor = enabledTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setSelectedTextColor(int selectedTextColor) {
            this.selectedTextColor = selectedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setPressedTextColor(int pressedTextColor) {
            this.pressedTextColor = pressedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setFocusedTextColor(int focusedTextColor) {
            this.focusedTextColor = focusedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setUnCheckableTextColor(int unCheckableTextColor) {
            this.unCheckableTextColor = unCheckableTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setUnCheckedTextColor(int unCheckedTextColor) {
            this.unCheckedTextColor = unCheckedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setUnEnabledTextColor(int unEnabledTextColor) {
            this.unEnabledTextColor = unEnabledTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setUnSelectedTextColor(int unSelectedTextColor) {
            this.unSelectedTextColor = unSelectedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setUnPressedTextColor(int unPressedTextColor) {
            this.unPressedTextColor = unPressedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setUnFocusedTextColor(int unFocusedTextColor) {
            this.unFocusedTextColor = unFocusedTextColor;
            this.textColorCount++;
            return this;
        }

        public Builder setBaseGradientDrawable(ShapeElement baseGradientDrawable) {
            this.baseGradientDrawable = baseGradientDrawable;
            return this;
        }

        public Builder setBaseStateListDrawable(StateElement baseStateListDrawable) {
            this.baseStateListDrawable = baseStateListDrawable;
            return this;
        }


        public Element build() {
            return hasSelectDrawable? getStateListDrawable() : getGradientDrawable();
        }

        public Builder buildTextColor(Component component) {
            Text text = null;
            if (component == null) {
                return this;
            }
            if (component instanceof Text) {
                text = (Text) component;
            }
            if (text == null) {
                return this;
            }
            handleTextColor(component);
            mClickCount = 0;

            text.setTouchEventListener(new Component.TouchEventListener() {
                @Override
                public boolean onTouchEvent(Component touchComponent, TouchEvent touchEvent) {
                    if (pressedTextColor !=null && unPressedTextColor!=null){
                        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                            component.setPressState(true);
                        } else {
                            component.setPressState(false);

                        }
                        handleTextColor(component);
                    }
                    if (selectedTextColor !=null && unSelectedTextColor!=null){
                        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                            mClickCount++;
                            if (mClickCount % 2 == 0) {
                                component.setSelected(false);
                            } else {
                                component.setSelected(true);

                            }
                            handleTextColor(component);
                        }

                    }
                        HiLog.info(label, "onTouchEvent pressed:%{public}d", touchEvent.getAction());
                        HiLog.info(label, "onTouchEvent pressed:%{public}b", component.isPressed());
                    return true;
                }
            });
            return this;
        }
        private void handleTextColor(Component component) {
            //处理文字颜色
            if (component instanceof Checkbox) {
                Checkbox checkbox = (Checkbox) component;
                if (checkedTextColor != null && unCheckedTextColor != null) {
                    checkbox.setTextColor(checkbox.isChecked() ? new Color(checkedTextColor) : new Color(unCheckedTextColor));
                    checkbox.setCheckedStateChangedListener(((absButton, b) -> checkbox.setTextColor(b ? new Color(checkedTextColor) : new Color(unCheckedTextColor))));
                }
            } else if (component instanceof RadioButton) {
                RadioButton radioButton = (RadioButton) component;
                if (checkedTextColor != null && unCheckedTextColor != null) {
                    radioButton.setTextColor(radioButton.isChecked() ? new Color(checkedTextColor) : new Color(unCheckedTextColor));
                    radioButton.setCheckedStateChangedListener(((absButton, b) -> radioButton.setTextColor(b ? new Color(checkedTextColor) : new Color(unCheckedTextColor))));
                }
            } else if (component instanceof Text) {
                Text text = (Text) component;
                if (pressedTextColor != null && unPressedTextColor != null) {
                    text.setTextColor(text.isPressed() ? new Color(pressedTextColor) : new Color(unPressedTextColor));
                } else if (selectedTextColor != null && unSelectedTextColor != null) {
                    text.setTextColor(text.isSelected() ? new Color(selectedTextColor) : new Color(unSelectedTextColor));
                } else if (enabledTextColor != null && unEnabledTextColor != null) {
                    text.setTextColor(text.isEnabled() ? new Color(enabledTextColor) : new Color(unEnabledTextColor));
                }
            }
        }

        private StateElement getStateListDrawable() {
            HiLog.info(label,"getStateListDrawable");
            StateElement stateListDrawable = baseStateListDrawable;
            stateListDrawable = getStateListDrawable(stateListDrawable);
            if (checkableDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, checkableDrawable);
            }
            if (unCheckableDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, unCheckableDrawable);
            }
            if (checkedDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, checkedDrawable);
            }
            if (unCheckedDrawable != null) {
                stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_CHECKED}, unCheckedDrawable);
            }
            if (enabledDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, enabledDrawable);
            }
            if (unEnabledDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_DISABLED}, unEnabledDrawable);
            }
            if (selectedDrawable != null) {
                HiLog.info(label,"stateListDrawable selectedDrawable");
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, selectedDrawable);
            }
            if (unSelectedDrawable != null) {
                HiLog.info(label,"stateListDrawable unSelectedDrawable");
                stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_SELECTED}, unSelectedDrawable);
            }
            if (pressedDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, pressedDrawable);
            }
            if (unPressedDrawable != null) {
                stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_PRESSED}, unPressedDrawable);
            }
            if (focusedDrawable != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED}, focusedDrawable);
            }
            if (unFocusedDrawable != null) {
                stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_FOCUSED}, unFocusedDrawable);
            }
            if (focusedHovered != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_HOVERED}, focusedHovered);
            }
            if (unFocusedHovered != null) {
                stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_HOVERED}, unFocusedHovered);
            }
            if (focusedActivated != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, focusedActivated);
            }
            if (unFocusedActivated != null) {
                stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, unFocusedActivated);
            }
            HiLog.info(label,"stateListDrawable count:%{public}d",stateListDrawable.getStateCount());
            return stateListDrawable;
        }

        private Element getGradientDrawable() {
            StateElement stateElement=new StateElement();
            ShapeElement defaultShape = getDefaultElement();
            ShapeElement updateShape = getDefaultElement();
            if (strokeWidth != null && strokeWidth > 0) {
                HiLog.info(label,"strokeWidth:%{public}d",strokeWidth);
                int start = 0;
                if (pressedStrokeColor != null && unPressedStrokeColor != null) {
                    HiLog.info(label,"pressedStrokeColor");
                    updateShape.setStroke(strokeWidth,RgbColor.fromArgbInt(pressedStrokeColor));
                    defaultShape.setStroke(strokeWidth,RgbColor.fromArgbInt(unPressedStrokeColor));
                    stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, updateShape);
                    stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_PRESSED}, defaultShape);

                }
                if (checkedStrokeColor != null && unCheckedStrokeColor != null) {
                    updateShape.setStroke(strokeWidth,RgbColor.fromArgbInt(checkedStrokeColor));
                    defaultShape.setStroke(strokeWidth,RgbColor.fromArgbInt(unCheckedStrokeColor));
                    stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, updateShape);
                    stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_CHECKED}, defaultShape);
                }
                if (enabledStrokeColor != null && unEnabledStrokeColor != null) {
                    updateShape.setStroke(strokeWidth,RgbColor.fromArgbInt(enabledStrokeColor));
                    defaultShape.setStroke(strokeWidth,RgbColor.fromArgbInt(unEnabledStrokeColor));
                    stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, updateShape);
                    stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_DISABLED}, defaultShape);
                }
                if (selectedStrokeColor != null && unSelectedStrokeColor != null) {
                    updateShape.setStroke(strokeWidth,RgbColor.fromArgbInt(selectedStrokeColor));
                    defaultShape.setStroke(strokeWidth,RgbColor.fromArgbInt(unSelectedStrokeColor));
                    stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, updateShape);
                    stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_SELECTED}, defaultShape);
                }
                if (focusedStrokeColor != null && unFocusedStrokeColor != null) {
                    updateShape.setStroke(strokeWidth,RgbColor.fromArgbInt(focusedStrokeColor));
                    defaultShape.setStroke(strokeWidth,RgbColor.fromArgbInt(unFocusedStrokeColor));
                    stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED}, updateShape);
                    stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_FOCUSED}, defaultShape);
                }
            }

            if (pressedSolidColor != null && unPressedSolidColor != null) {
                updateShape.setRgbColor(RgbColor.fromArgbInt(pressedSolidColor));
                defaultShape.setRgbColor(RgbColor.fromArgbInt(unPressedSolidColor));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, updateShape);
                stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_PRESSED}, defaultShape);
            }

            if (checkedSolidColor != null && unCheckedSolidColor != null) {
                updateShape.setRgbColor(RgbColor.fromArgbInt(checkedSolidColor));
                defaultShape.setRgbColor(RgbColor.fromArgbInt(unCheckedSolidColor));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, updateShape);
                stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_CHECKED}, defaultShape);
            }
            if (enabledSolidColor != null && unEnabledSolidColor != null) {
                updateShape.setRgbColor(RgbColor.fromArgbInt(enabledSolidColor));
                defaultShape.setRgbColor(RgbColor.fromArgbInt(unEnabledSolidColor));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, updateShape);
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_DISABLED}, defaultShape);
            }
            if (selectedSolidColor != null && unSelectedSolidColor != null) {
                updateShape.setRgbColor(RgbColor.fromArgbInt(selectedSolidColor));
                defaultShape.setRgbColor(RgbColor.fromArgbInt(unSelectedSolidColor));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, updateShape);
                stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_SELECTED}, defaultShape);
            }
            if (focusedSolidColor != null && unFocusedSolidColor != null) {
                updateShape.setRgbColor(RgbColor.fromArgbInt(focusedSolidColor));
                defaultShape.setRgbColor(RgbColor.fromArgbInt(unFocusedSolidColor));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED}, updateShape);
                stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_FOCUSED}, defaultShape);;
            }

            HiLog.info(label,"stateElement count:%{public}d",stateElement.getStateCount());
            return (stateElement.getStateCount()>0)?stateElement:getDefaultElement() ;
        }
        ShapeElement getDefaultElement(){
            ShapeElement drawable = baseGradientDrawable;
            if (drawable == null) {
                drawable = new ShapeElement();
            }
            HiLog.info(label,"shape:%{public}d",shape.value);
            drawable.setShape(shape.value);

            if (cornersRadius != null) {
                HiLog.info(label,"setCornerRadius:%{public}f",cornersRadius);
                drawable.setCornerRadius(cornersRadius);
            }

            if (cornersBottomLeftRadius != null && cornersBottomRightRadius != null &&
                    cornersTopLeftRadius != null && cornersTopRightRadius != null) {
                //初始化圆角个数
                float[] cornerRadius = new float[8];
                cornerRadius[0] = cornersTopLeftRadius;
                cornerRadius[1] = cornersTopLeftRadius;
                cornerRadius[2] = cornersTopRightRadius;
                cornerRadius[3] = cornersTopRightRadius;
                cornerRadius[4] = cornersBottomRightRadius;
                cornerRadius[5] = cornersBottomRightRadius;
                cornerRadius[6] = cornersBottomLeftRadius;
                cornerRadius[7] = cornersBottomLeftRadius;
                drawable.setCornerRadiiArray(cornerRadius);
            }

            // 处理渐变角度，非默认-1值，每过46度，确定渐变方向
            if (gradient == Gradient.Linear && gradientAngle != -1) {
                gradientAngle %= 360;
                if (gradientAngle % 45 == 0) {
                    ShapeElement.Orientation mOrientation = ShapeElement.Orientation.LEFT_TO_RIGHT;
                    switch (gradientAngle) {
                        case 0:
                            mOrientation = ShapeElement.Orientation.LEFT_TO_RIGHT;
                            break;
                        case 45:
                            mOrientation = ShapeElement.Orientation.BOTTOM_LEFT_TO_TOP_RIGHT;
                            break;
                        case 90:
                            mOrientation = ShapeElement.Orientation.BOTTOM_TO_TOP;
                            break;
                        case 135:
                            mOrientation = ShapeElement.Orientation.BOTTOM_RIGHT_TO_TOP_LEFT;
                            break;
                        case 180:
                            mOrientation = ShapeElement.Orientation.RIGHT_TO_LEFT;
                            break;
                        case 225:
                            mOrientation = ShapeElement.Orientation.TOP_RIGHT_TO_BOTTOM_LEFT;
                            break;
                        case 270:
                            mOrientation = ShapeElement.Orientation.TOP_TO_BOTTOM;
                            break;
                        case 315:
                            mOrientation = ShapeElement.Orientation.TOP_LEFT_TO_BOTTOM_RIGHT;
                            break;
                    }
                    drawable.setGradientOrientation(mOrientation);
                }
            }

            if (gradientCenterX != null && gradientCenterY != null) {
//                drawable.setGradientCenter(gradientCenterX, gradientCenterY);
            }
            if (gradientStartColor != null && gradientEndColor != null) {
                RgbColor[] colors;
                if (gradientCenterColor != null) {
                    colors = new RgbColor[3];
                    colors[0] = RgbColor.fromArgbInt(gradientStartColor);
                    colors[1] = RgbColor.fromArgbInt(gradientCenterColor);
                    colors[2] = RgbColor.fromArgbInt(gradientEndColor);
                } else {
                    colors = new RgbColor[2];
                    colors[0] = RgbColor.fromArgbInt(gradientStartColor);
                    colors[1] = RgbColor.fromArgbInt(gradientEndColor);
                }
                drawable.setRgbColors(colors);
            }
            if (gradientRadius != null) {
//                drawable.setGradientRadius(gradientRadius);
            }
            drawable.setShaderType(gradient.value);

            if (!padding.isEmpty()) {
                drawable.setBounds(padding.left, padding.top, padding.right, padding.bottom);
            }
            if (sizeWidth != null && sizeHeight != null) {
//                drawable.setSize(sizeWidth.intValue(), sizeHeight.intValue());
            }
            if (strokeWidth>0 && strokeColor !=null){
                drawable.setStroke(strokeWidth,RgbColor.fromArgbInt(strokeColor));
            }
            if (strokeDashGap != 0 || strokeDashWidth!=0) {
                drawable.setDashPathEffectValues(new float[]{strokeDashWidth,strokeDashGap}, -100);
            }
            if (solidColor != null) {
                drawable.setRgbColor(RgbColor.fromArgbInt(new Color(solidColor).getValue()));
            }
            return drawable;
        }
        StateElement getStateListDrawable(StateElement stateListDrawable) {
            if (stateListDrawable == null) {
                stateListDrawable = new StateElement();
            }
            return stateListDrawable;
        }
    }

     //设置drawable的位置
    public static void setDrawable(Element drawable, Component view, DrawablePosition drawablePosition) {
        HiLog.info(label,"setDrawable");
        if (view instanceof Text) {
            if(drawable != null){
                HiLog.info(label,"setAroundElements");
                if (drawablePosition == DrawablePosition.Left) {
                    ((Text) view).setAroundElements(drawable, null, null, null);
                } else if (drawablePosition == DrawablePosition.Top) {
                    ((Text) view).setAroundElements(null, drawable, null, null);
                } else if (drawablePosition == DrawablePosition.Right) {
                    ((Text) view).setAroundElements(null, null, drawable, null);
                } else if (drawablePosition == DrawablePosition.Bottom) {
                    ((Text) view).setAroundElements(null, null, null, drawable);
                } else {
                    view.setBackground(drawable);
                }
            }else {
                //清空view的aroundElements;
                Element[] drawables = ((Text) view).getAroundElements();
                if (drawablePosition == DrawablePosition.Left) {
                    drawables[0] = null;
                } else if (drawablePosition == DrawablePosition.Top) {
                    drawables[1] = null;
                } else if (drawablePosition == DrawablePosition.Right) {
                    drawables[2] = null;
                } else if (drawablePosition == DrawablePosition.Bottom) {
                    drawables[3] = null;

                }
                ((Text) view).setAroundElements(drawables[0], drawables[1], drawables[2], drawables[3]);
            }
        } else {
            view.setBackground(drawable);
        }

    }
}
