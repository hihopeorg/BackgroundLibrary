package com.noober.background.drawable;

import com.noober.background.view.BLComponentConstant;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatterException;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class GradientDrawableCreator implements ICreateDrawable {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000030, "GradientDrawableCreator");
    private AttrSet mAttrSet;

    public GradientDrawableCreator(AttrSet attrSet) {
        this.mAttrSet = attrSet;
    }

    @Override
    public ShapeElement create() throws LayoutScatterException {
        HiLog.info(label,"GradientDrawableCreator");
        ShapeElement shapeElement = new ShapeElement();
        RgbColor[] gradientColors = new RgbColor[]{
                new RgbColor(), new RgbColor(), new RgbColor()};
        Color startGradientColor =null;
        Color centerGradientColor =null;
        Color endGradientColor=null;
        int strokeWidth=0;
        Color solidColor =null;
        Color strokeColor= null;
        int coreRadius = 0;
        int strokeDashGap = 0;
        int strokeDashWidth =0;
        int gradientAngle=0;
        String shape =null;
        int gradientType=0;


        if (mAttrSet.getAttr(BLComponentConstant.BL_STROKE_WIDTH).isPresent()) {
            strokeWidth = mAttrSet.getAttr(BLComponentConstant.BL_STROKE_WIDTH).get().getDimensionValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_SOLID_COLOR).isPresent()) {
            solidColor = mAttrSet.getAttr(BLComponentConstant.BL_SOLID_COLOR).get().getColorValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_STROKE_COLOR).isPresent()) {
            strokeColor = mAttrSet.getAttr(BLComponentConstant.BL_STROKE_COLOR).get().getColorValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_CORE_RADIUS).isPresent()) {
            coreRadius = mAttrSet.getAttr(BLComponentConstant.BL_CORE_RADIUS).get().getDimensionValue();
        }

        if (mAttrSet.getAttr(BLComponentConstant.BL_STROKE_DASHGAP).isPresent()) {
            strokeDashGap = mAttrSet.getAttr(BLComponentConstant.BL_STROKE_DASHGAP).get().getDimensionValue();
        }

        if (mAttrSet.getAttr(BLComponentConstant.BL_STROKE_DASHWIDTH).isPresent()) {
            strokeDashWidth = mAttrSet.getAttr(BLComponentConstant.BL_STROKE_DASHWIDTH).get().getDimensionValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_TYPE).isPresent()){
            gradientType = mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_TYPE).get().getIntegerValue();
        }

        if (mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_START_COLOR).isPresent()) {
            startGradientColor = mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_START_COLOR).get().getColorValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_CENTER_COLOR).isPresent()) {
            centerGradientColor = mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_CENTER_COLOR).get().getColorValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_END_COLOR).isPresent()) {
            endGradientColor = mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_END_COLOR).get().getColorValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_ANGEL).isPresent()) {
            gradientAngle = mAttrSet.getAttr(BLComponentConstant.BL_GRADIENT_ANGEL).get().getIntegerValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_SHAPE).isPresent()) {
            shape = mAttrSet.getAttr(BLComponentConstant.BL_SHAPE).get().getStringValue();
        }

        if (solidColor != null) {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(solidColor.getValue()));
        }
        shapeElement.setCornerRadius(coreRadius);

        if (strokeColor != null && strokeWidth != 0) {
            shapeElement.setStroke(strokeWidth, RgbColor.fromArgbInt(strokeColor.getValue()));
        }
        if (strokeDashGap != 0) {
            shapeElement.setDashPathEffectValues(new float[]{strokeDashWidth,strokeDashGap}, -100);
        }
        shapeElement.setShape(getBLShape(shape));
        //gradient；

        if (gradientType == ShapeElement.LINEAR_GRADIENT_SHADER_TYPE && gradientAngle != -1) {
            gradientAngle %= 360;
            if (gradientAngle % 45 == 0) {
                ShapeElement.Orientation mOrientation = ShapeElement.Orientation.LEFT_TO_RIGHT;
                switch (gradientAngle) {
                    case 0:
                        mOrientation = ShapeElement.Orientation.LEFT_TO_RIGHT;
                        break;
                    case 45:
                        mOrientation = ShapeElement.Orientation.BOTTOM_LEFT_TO_TOP_RIGHT;
                        break;
                    case 90:
                        mOrientation = ShapeElement.Orientation.BOTTOM_TO_TOP;
                        break;
                    case 135:
                        mOrientation = ShapeElement.Orientation.BOTTOM_RIGHT_TO_TOP_LEFT;
                        break;
                    case 180:
                        mOrientation = ShapeElement.Orientation.RIGHT_TO_LEFT;
                        break;
                    case 225:
                        mOrientation = ShapeElement.Orientation.TOP_RIGHT_TO_BOTTOM_LEFT;
                        break;
                    case 270:
                        mOrientation = ShapeElement.Orientation.TOP_TO_BOTTOM;
                        break;
                    case 315:
                        mOrientation = ShapeElement.Orientation.TOP_LEFT_TO_BOTTOM_RIGHT;
                        break;
                }
                shapeElement.setGradientOrientation(mOrientation);
            }
        }

        shapeElement.setShaderType(gradientType);
        if (startGradientColor != null && endGradientColor != null) {
            RgbColor[] colors;

            //初始化渐变颜色，渐变中心色不为空，rgbColor[]长度为3
            if (centerGradientColor != null) {
                colors = new RgbColor[3];
                colors[0] = RgbColor.fromArgbInt(startGradientColor.getValue());
                colors[1] = RgbColor.fromArgbInt(centerGradientColor.getValue());
                colors[2] = RgbColor.fromArgbInt(endGradientColor.getValue());
            } else {
                colors = new RgbColor[2];
                colors[0] = RgbColor.fromArgbInt(startGradientColor.getValue());
                colors[1] = RgbColor.fromArgbInt(endGradientColor.getValue());
            }
            shapeElement.setRgbColors(colors);
        }
        return shapeElement;
    }

    private int getBLShape(String shape) {
        if (BLComponentConstant.BL_RECTANGLE.equals(shape)) {
            return ShapeElement.RECTANGLE;
        } else if (BLComponentConstant.BL_OVAL.equals(shape)) {
            return ShapeElement.OVAL;
        } else if (BLComponentConstant.BL_LINE.equals(shape)) {
            return ShapeElement.LINE;
        } else if (BLComponentConstant.BL_RING.equals(shape)) {
            return ShapeElement.RECTANGLE;
        } else {
            return ShapeElement.RECTANGLE;
        }
    }

}
