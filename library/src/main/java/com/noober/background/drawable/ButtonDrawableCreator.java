package com.noober.background.drawable;

import com.noober.background.view.BLComponentConstant;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentState;
import ohos.agp.components.LayoutScatterException;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class ButtonDrawableCreator implements ICreateDrawable {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x000032, "ButtonDrawableCreator");
    private AttrSet mAttrSet;

    private StateElement mStateElement;
    private int mStrokeWidth;

    public ButtonDrawableCreator(AttrSet attrSet) {
        this.mAttrSet = attrSet;
    }

    @Override
    public StateElement create() throws LayoutScatterException {
        mStateElement = new StateElement();
        Color pressStrokeColor;
        Color unPressStrokeColor;

        Color pressColor;
        Color unPressColor;

        Color checkedColor;
        Color unCheckedColor;
        Color checkedStrokeColor;
        Color unCheckedStrokeColor;

        Color selectedColor;
        Color unSelectedColor;
        Color selectedStrokeColor;
        Color unSelectedStrokeColor;

        Color enabledColor, unEnabledColor, enabledStrokeColor, unEnabledStrokeColor;
        Color focusedColor, unFocusedColor, focusedStrokeColor, unFocusedStrokeColor;


        if (mAttrSet.getAttr(BLComponentConstant.BL_STROKE_WIDTH).isPresent()) {
            mStrokeWidth = mAttrSet.getAttr(BLComponentConstant.BL_STROKE_WIDTH).get().getDimensionValue();
        }
        pressStrokeColor = getColor(BLComponentConstant.BL_PRESSED_STROKE_COLOR);
        unPressStrokeColor = getColor(BLComponentConstant.BL_UNPRESSED_STROKE_COLOR);
        unPressColor = getColor(BLComponentConstant.BL_UNPRESSED_SOLID_COLOR);
        pressColor = getColor(BLComponentConstant.BL_PRESSED_SOLID_COLOR);
        checkedColor = getColor(BLComponentConstant.BL_CHECKED_SOLID_COLOR);
        unCheckedColor = getColor(BLComponentConstant.BL_UNCHECKED_SOLID_COLOR);
        checkedStrokeColor = getColor(BLComponentConstant.BL_CHECKED_STROKE_COLOR);
        unCheckedStrokeColor = getColor(BLComponentConstant.BL_UNCHECKED_STROKE_COLOR);

        selectedColor = getColor(BLComponentConstant.BL_SELECTED_SOLID_COLOR);
        unSelectedColor = getColor(BLComponentConstant.BL_UNSELECTED_SOLID_COLOR);
        selectedStrokeColor = getColor(BLComponentConstant.BL_SELECTED_STROKE_COLOR);
        unSelectedStrokeColor = getColor(BLComponentConstant.BL_UNSELECTED_STROKE_COLOR);

        enabledColor = getColor(BLComponentConstant.BL_ENABLED_SOLID_COLOR);
        unEnabledColor = getColor(BLComponentConstant.BL_UNENABLED_SOLID_COLOR);
        enabledStrokeColor = getColor(BLComponentConstant.BL_ENABLED_STROKE_COLOR);
        unEnabledStrokeColor = getColor(BLComponentConstant.BL_UNENABLED_STROKE_COLOR);

        focusedColor = getColor(BLComponentConstant.BL_FOCUSED_SOLID_COLOR);
        unFocusedColor = getColor(BLComponentConstant.BL_UNFOCUSED_SOLID_COLOR);
        focusedStrokeColor = getColor(BLComponentConstant.BL_FOCUSED_STROKE_COLOR);
        unFocusedStrokeColor = getColor(BLComponentConstant.BL_UNFOCUSED_STROKE_COLOR);


        if (pressColor != null && unPressColor != null) {
            setStateElement(0, ComponentState.COMPONENT_STATE_PRESSED, unPressStrokeColor,
                    pressStrokeColor, unPressColor, pressColor);
        }
        if (checkedColor != null && unCheckedColor != null) {
            setStateElement(0, ComponentState.COMPONENT_STATE_CHECKED, unCheckedStrokeColor,
                    checkedStrokeColor, unCheckedColor, checkedColor);
        }
        if (selectedColor != null && unSelectedColor != null) {
            setStateElement(0, ComponentState.COMPONENT_STATE_SELECTED, unSelectedStrokeColor,
                    selectedStrokeColor, unSelectedColor, selectedColor);
        }

        if (enabledColor != null && unEnabledColor != null) {
            setStateElement(ComponentState.COMPONENT_STATE_DISABLED, 0, unEnabledStrokeColor,
                    enabledStrokeColor, unEnabledColor, enabledColor);
        }
        if (focusedColor != null && unFocusedColor != null) {
            setStateElement(0, ComponentState.COMPONENT_STATE_FOCUSED, unFocusedStrokeColor,
                    focusedStrokeColor, unEnabledColor, enabledColor);
        }

        return mStateElement;
    }

    private void setStateElement(int defaultComponentState, int updateComponentState, Color defaultStrokeColor, Color updateStrokeColor,
                                 Color defaultElementColor, Color updateElementColor) {
        ShapeElement updateElement = DrawableFactory.getDrawable(mAttrSet);
        ShapeElement defaultElement = DrawableFactory.getDrawable(mAttrSet);
        if (updateStrokeColor != null) {
            updateElement.setStroke(mStrokeWidth, RgbColor.fromArgbInt(updateStrokeColor.getValue()));
        }
        if (defaultStrokeColor != null) {
            defaultElement.setStroke(mStrokeWidth, RgbColor.fromArgbInt(defaultStrokeColor.getValue()));
        }
        updateElement.setRgbColor(RgbColor.fromArgbInt(updateElementColor.getValue()));
        defaultElement.setRgbColor(RgbColor.fromArgbInt(defaultElementColor.getValue()));
        mStateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY, updateComponentState}, updateElement);
        mStateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY, defaultComponentState}, defaultElement);
    }

    private Color getColor(String attrKey) {
        if (mAttrSet.getAttr(attrKey).isPresent()) {
            return mAttrSet.getAttr(attrKey).get().getColorValue();
        }
        return null;
    }

}
