package com.noober.background.drawable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatterException;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;

public class DrawableFactory {

    //获取shape属性的drawable
    public static ShapeElement getDrawable(AttrSet attrSet) throws LayoutScatterException {
        return new GradientDrawableCreator(attrSet).create();
    }

    //获取AnimationDrawable属性的drawable
    public static FrameAnimationElement getAnimationDrawable(AttrSet attrSet) throws LayoutScatterException{
        return new AnimationDrawableCreator(attrSet).create();
    }

    //获取button 属性的drawable
    public static StateElement getButtonDrawable(AttrSet attrSet) throws LayoutScatterException {
        return new ButtonDrawableCreator(attrSet).create();
    }
}
