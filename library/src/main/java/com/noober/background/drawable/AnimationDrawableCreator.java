package com.noober.background.drawable;

import com.noober.background.view.BLComponentConstant;
import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatterException;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.PixelMapElement;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class AnimationDrawableCreator implements ICreateDrawable{
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x000031, "AnimationDrawableCreator");

    private AttrSet mAttrSet;

    public AnimationDrawableCreator (AttrSet attrSet){
        this.mAttrSet = attrSet;
    }
    @Override
    public FrameAnimationElement create() throws LayoutScatterException {
        FrameAnimationElement frameAnimationElement = new FrameAnimationElement();
        PixelMapElement pixelMapElement =null;
        boolean mIsOneShot = false;
        int animDuration = 0;
        boolean mIsAutoStart =false;
        if (mAttrSet.getAttr(BLComponentConstant.BL_DURATION).isPresent()) {
            animDuration = mAttrSet.getAttr(BLComponentConstant.BL_DURATION).get().getIntegerValue();
        }

        if (mAttrSet.getAttr(BLComponentConstant.BL_ONESHAOT).isPresent()) {
            mIsOneShot = mAttrSet.getAttr(BLComponentConstant.BL_ONESHAOT).get().getBoolValue();
        }
        if (mAttrSet.getAttr(BLComponentConstant.BL_ANIM_AUTO_START).isPresent()){
            mIsAutoStart = mAttrSet.getAttr(BLComponentConstant.BL_ANIM_AUTO_START).get().getBoolValue();
        }

        for (int i = 0; ; i++) {
            if (mAttrSet.getAttr(BLComponentConstant.BL_FRAME_DRAWABLE_ITEM + i).isPresent()) {
                pixelMapElement = (PixelMapElement) mAttrSet.getAttr(BLComponentConstant.BL_FRAME_DRAWABLE_ITEM + i).get().getElement();
                HiLog.info(label, "add frame:${public}d",i);
                frameAnimationElement.addFrame(pixelMapElement,animDuration);
            }else {
                break;
            }
        }
        frameAnimationElement.setOneShot(mIsOneShot);;
        if (mIsAutoStart) {
            frameAnimationElement.start();
        }
        return frameAnimationElement;
    }
}
