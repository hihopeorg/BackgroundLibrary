package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.TextField;
import ohos.app.Context;

public class BLEditText extends TextField {
    public BLEditText(Context context) {
        super(context);
    }

    public BLEditText(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLEditText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
