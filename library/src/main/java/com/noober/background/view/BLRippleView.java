/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noober.background.view;

import ohos.agp.components.*;

import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;


public class BLRippleView extends BLTextView implements Component.DrawTask, Component.TouchEventListener {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x000030, "BLRippleView");
    private Paint mPaint;

    private int mRadiusMax;

    private int mRadius;

    private boolean mIsAnimating;

    private Color mRippleColor;
    private Color mBackgroundColor=Color.BLACK;
    private int mSpeed = 20;

    private float mCenterX;
    private float mCenterY;

    private EventHandler mEventHandler;

    private EventRunner mEventRunner;

    private Paint mBackgroundPaint;

    private RectFloat mRectFloat;

    private Paint mTextPaint;

    private AttrSet mAttrSet;

    private int mStrokeRadius;


    public BLRippleView(Context context) {
        super(context);
        initPaint();
    }

    public BLRippleView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
        initPaint();
    }

    public BLRippleView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
        initPaint();
    }

    private void init(AttrSet attrSet) {
        mAttrSet = attrSet;
        if (attrSet.getAttr(BLComponentConstant.BL_RIPPLE_COLOR).isPresent()) {
            mRippleColor = attrSet.getAttr(BLComponentConstant.BL_RIPPLE_COLOR).get().getColorValue();
        }

        if (attrSet.getAttr(BLComponentConstant.BL_RIPPLE_SPEED).isPresent()) {
            mSpeed = attrSet.getAttr(BLComponentConstant.BL_RIPPLE_SPEED).get().getIntegerValue();
        }

        if (attrSet.getAttr(BLComponentConstant.BL_RIPPLE_BACKGROUND).isPresent()) {
            mBackgroundColor = attrSet.getAttr(BLComponentConstant.BL_RIPPLE_BACKGROUND).get().getColorValue();
        }

        if (mAttrSet.getAttr(BLComponentConstant.BL_CORE_RADIUS).isPresent()) {
            mStrokeRadius = mAttrSet.getAttr(BLComponentConstant.BL_CORE_RADIUS).get().getDimensionValue();
        }
    }


    @Override
    public void postLayout() {
        super.postLayout();
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(mRippleColor == null ? Color.WHITE : mRippleColor);
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setColor(mBackgroundColor);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTextSize(50);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
        mEventHandler = new EventHandler(EventRunner.current()) {
            @Override
            public void distributeEvent(InnerEvent event) {
                invalidate();
                super.distributeEvent(event);
            }
        };


    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        mRadiusMax = Math.max(getHeight(), getWidth());

        HiLog.info(label, "getWidth:%{public}d,getHeight:%{public}d", getWidth(), getHeight());
        int textWidth = 0;
        mRectFloat = new RectFloat(0, 0, getWidth(), getHeight());
        canvas.drawRect(mRectFloat,mBackgroundPaint);
        if (!TextTool.isNullOrEmpty(getText())){
            textWidth = getText().length()*getTextSize();
            canvas.drawText(mTextPaint,getText(), (getWidth() -textWidth) / 2, getHeight() / 2);
        }
        if (!mIsAnimating) {
            return;
        }
        if (mIsAnimating && mRadius > mRadiusMax) {
            mIsAnimating = false;
            mRadius = 0;
            return;
        }
        mRadius += mSpeed;
        HiLog.info(label, "radius:%{public}d", mRadius);
        canvas.drawCircle(mCenterX, mCenterY, mRadius, mPaint);
        if (!TextTool.isNullOrEmpty(getText())){
            canvas.drawText(mTextPaint,getText(), (getWidth() -textWidth) / 2, getHeight() / 2);
        }

        HiLog.info(label, "onDraw");
        mEventHandler.sendEvent(100, 0);
    }

    @Override
    public void invalidate() {
        addDrawTask(this::onDraw);
        super.invalidate();

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        MmiPoint event = touchEvent.getPointerPosition(0);
        Rect rect = component.getComponentPosition();
        HiLog.info(label, "event:%{public}f,down:%{public}f", event.getX(), event.getY());
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            HiLog.info(label, "onTouchEvent down");
            mCenterX = event.getX();
            mCenterY = event.getY() - (rect.getCenterY() - component.getHeight() / 2);
            mRadius = 0;
            mIsAnimating = true;
        }
        mEventHandler.sendEvent(100, 0);
        return true;
    }

}
