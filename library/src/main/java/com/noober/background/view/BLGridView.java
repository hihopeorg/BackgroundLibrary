package com.noober.background.view;

import ohos.agp.components.*;
import ohos.app.Context;

public class BLGridView extends ListContainer {

    private int mColumnCount;
    private int mRowCount;
    private TableLayoutManager mTableLayoutManager;

    public BLGridView(Context context) {
        super(context);
    }

    public BLGridView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
        initTask(attrSet);
    }

    public BLGridView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
        initTask(attrSet);
    }

    private void init(AttrSet attrSet) {
        if (attrSet.getAttr(BLComponentConstant.BL_GRID_COLUMN_COUNT).isPresent()) {
            mColumnCount = attrSet.getAttr(BLComponentConstant.BL_GRID_COLUMN_COUNT).get().getIntegerValue();
        }
        if (attrSet.getAttr(BLComponentConstant.BL_GRID_ROW_COUNT).isPresent()) {
            mRowCount = attrSet.getAttr(BLComponentConstant.BL_GRID_ROW_COUNT).get().getIntegerValue();
        }
        mTableLayoutManager = new TableLayoutManager();
        mTableLayoutManager.setColumnCount(mColumnCount);
        mTableLayoutManager.setRowCount(mRowCount);
        this.setLayoutManager(mTableLayoutManager);
    }

    private void  initTask(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }

}
