/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noober.background.view;

public interface BLComponentConstant {
    String BL_LEFT = "left";
    String BL_TOP = "top";
    String BL_RIGHT = "right";
    String BL_BOTTOM = "bottom";
    String BL_RECTANGLE = "rectangle";
    String BL_OVAL = "oval";
    String BL_LINE = "line";
    String BL_RING = "ring";
    String BL_SOLID_COLOR = "bl_solid_color";
    String BL_STROKE_COLOR = "bl_stroke_color";
    String BL_STROKE_WIDTH = "bl_stroke_width";
    String BL_CORE_RADIUS = "bl_corners_radius";
    String BL_PADDING_BOTTOM = "bl_padding_bottom";
    String BL_PADDING_LEFT = "bl_padding_left";
    String BL_PADDING_RIGHT = "bl_padding_right";
    String BL_PADDING_TOP = "bl_padding_top";
    String BL_STROKE_DASHGAP = "bl_stroke_dashGap";
    String BL_STROKE_DASHWIDTH = "bl_stroke_dashWidth";
    String BL_STROKE_POSITION = "bl_stroke_position";

    String BL_SHAPE = "bl_shape";
    String BL_GRADIENT_ANGEL = "bl_gradient_angle";
    String BL_GRADIENT_CENTER_COLOR = "bl_gradient_centerColor";
    String BL_GRADIENT_END_COLOR = "bl_gradient_endColor";
    String BL_GRADIENT_START_COLOR = "bl_gradient_startColor";
    String BL_GRADIENT_TYPE = "bl_gradient_type";
    String BL_ANIM_AUTO_START = "bl_anim_auto_start";
    String BL_DURATION = "bl_duration";
    String BL_ONESHAOT = "bl_oneshot";
    String BL_FRAME_DRAWABLE_ITEM = "bl_frame_drawable_item";
    String BL_GRID_COLUMN_COUNT= "bl_grid_column_count";
    String BL_GRID_ROW_COUNT = "bl_grid_row_count";
    String BL_RIPPLE_COLOR ="bl_ripple_color";
    String BL_RIPPLE_SPEED = "bl_ripple_speed";
    String BL_RIPPLE_BACKGROUND = "bl_ripple_background";


    String BL_PRESS_TEXT_COLOR = "bl_pressed_textColor";
    String BL_UNPRESS_TEXT_COLOR= "bl_unPressed_textColor";
    String BL_SELECTED_TEXT_COLOR = "bl_selected_textColor";
    String BL_UNSELECTED_TEXT_COLOR = "bl_unSelected_textColor";


    String BL_CHECKED_TEXT_COLOR = "bl_checked_textColor";
    String BL_UNCHECKED_TEXT_COLOR = "bl_unChecked_textColor";
    String BL_ENABLED_TEXT_COLOR = "bl_enabled_textColor";
    String BL_UNENABLED_TEXT_COLOR = "bl_unEnabled_textColor";
    String BL_CHECKABLE_TEXT_COLOR = "bl_checkable_textColor";
    String BL_UNCHECKABLE_TEXT_COLOR = "bl_unCheckable_textColor";
    String BL_FOCUSED_TEXT_COLOR = "bl_focused_textColor";
    String BL_UNFOCUSED_TEXT_COLOR = "bl_unFocused_textColor";



    String BL_PRESSED_STROKE_COLOR = "bl_pressed_stroke_color";
    String BL_UNPRESSED_STROKE_COLOR = "bl_unPressed_stroke_color";


    String BL_UNPRESSED_SOLID_COLOR = "bl_unPressed_solid_color";
    String BL_PRESSED_SOLID_COLOR = "bl_pressed_solid_color";

    String BL_CHECKED_SOLID_COLOR = "bl_checked_solid_color";
    String BL_UNCHECKED_SOLID_COLOR = "bl_unChecked_solid_color";


    String BL_ENABLED_SOLID_COLOR = "bl_enabled_solid_color";
    String BL_UNENABLED_SOLID_COLOR = "bl_unEnabled_solid_color";

    String BL_SELECTED_SOLID_COLOR = "bl_selected_solid_color";
    String BL_UNSELECTED_SOLID_COLOR = "bl_unSelected_solid_color";


    String BL_FOCUSED_SOLID_COLOR = "bl_focused_solid_color";
    String BL_UNFOCUSED_SOLID_COLOR = "bl_unFocused_solid_color";

    String BL_CHECKED_STROKE_COLOR = "bl_checked_stroke_color";
    String BL_UNCHECKED_STROKE_COLOR = "bl_unChecked_stroke_color";

    String BL_ENABLED_STROKE_COLOR = "bl_enabled_stroke_color";
    String BL_UNENABLED_STROKE_COLOR = "bl_unEnabled_stroke_color";


    String BL_SELECTED_STROKE_COLOR = "bl_selected_stroke_color";
    String BL_UNSELECTED_STROKE_COLOR = "bl_unSelected_stroke_color";

    String BL_FOCUSED_STROKE_COLOR = "bl_focused_stroke_color";
    String BL_UNFOCUSED_STROKE_COLOR = "bl_unFocused_stroke_color";

    String BL_CHECKED_BUTTON_DRAWABLE = "bl_checked_button_drawable";
    String BL_UNCHECKED_BUTTON_DRAWABLE = "bl_unChecked_button_drawable";

    String BL_DRAWABLE_POSITION = "bl_position";

    String BL_UNPRESSED_DRAWABLE = "bl_unPressed_drawable";
    String BL_PRESSED_DRAWABLE = "bl_pressed_drawable";


    String BL_SELECTED_DRAWABLE = "bl_selected_drawable";
    String BL_UNSELECTED_DRAWABLE = "bl_unSelected_drawable";

    String BL_CHECKED_DRAWABLE = "bl_checked_drawable";
    String BL_UNCHECKED_DRAWABLE = "bl_unChecked_drawable";

    String BL_ENABLED_DRAWABLE = "bl_enabled_drawable";
    String BL_UNENABLED_DRAWABLE = "bl_unEnabled_drawable";

    String BL_FOCUSED_DRAWABLE = "bl_focused_drawable";
    String BL_UNFOCUSED_DRAWABLE = "bl_unFocused_drawable";


}
