package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;

public class BLDependentLayout extends DependentLayout {
    public BLDependentLayout(Context context) {
        super(context);
    }

    public BLDependentLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLDependentLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
