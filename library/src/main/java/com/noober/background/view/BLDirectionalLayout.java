package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class BLDirectionalLayout extends DirectionalLayout {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x000002, "BLDirectionalLayout");
    public BLDirectionalLayout(Context context) {
        super(context);
    }

    public BLDirectionalLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLDirectionalLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
