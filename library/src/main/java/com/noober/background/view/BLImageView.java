package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.app.Context;

public class BLImageView extends Image {
    public BLImageView(Context context) {
        super(context);
    }

    public BLImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
