package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.render.layoutboost.LayoutBoost;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.solidxml.SolidXml;

public class BLStackLayout extends StackLayout {
    public BLStackLayout(Context context) {
        super(context);
    }

    public BLStackLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLStackLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
