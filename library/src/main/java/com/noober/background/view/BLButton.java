package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.app.Context;

public class BLButton extends Button {
    public BLButton(Context context) {
        super(context);
    }

    public BLButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
