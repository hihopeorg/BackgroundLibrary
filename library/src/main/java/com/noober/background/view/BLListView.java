package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

public class BLListView extends ListContainer {
    public BLListView(Context context) {
        super(context);
    }

    public BLListView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLListView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
