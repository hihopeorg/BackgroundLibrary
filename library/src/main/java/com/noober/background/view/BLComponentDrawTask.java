/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noober.background.view;


import com.noober.background.common.BLDrawableTool;
import com.noober.background.drawable.DrawableFactory;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.TextTool;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

public class BLComponentDrawTask implements Component.DrawTask, Component.TouchEventListener {

    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000100, "BLComponentDrawTask");

    private int mClickCount;

    private int mAroundCount;

    private int mStrokeWidth;

    private Color mStrokeColor;

    private Paint mPaint = new Paint();

    private String mStrokePosition;

    private String mMapElementPosition;


    private Component mComponent;

    private static BLComponentDrawTask mBlComponentDrawTask;

    private int mAroundPadding;
    private Element mSelectedElement, mUnselectedElement;
    private Element mCheckedElement, mUnCheckedElement;
    private Element mPressMapElement, mUnPressMapElement;
    private Element mEnabledMapElement, mUnEnabledMapElement;


    private Color mSelectedTextColor, mUnSelectedTextColor;
    private Color mPressedTextColor, mUnpressedTextColor;
    private Color mEnabledTextColor, mUnEnabledTextColor;
    private Color mCheckedTextColor, mUnCheckedTextColor;


    private AttrSet mAttrSet;


    public BLComponentDrawTask() {

    }

    public static BLComponentDrawTask getInstance() {
        if (mBlComponentDrawTask == null) {
            mBlComponentDrawTask = new BLComponentDrawTask();
        }
        return mBlComponentDrawTask;
    }

    public void setBackgroundDrawable(Component component, AttrSet attrSet) {
        mComponent = component;
        init(attrSet);
    }

    private void init(AttrSet attrs) {
        mClickCount = 0;
        mAroundCount = 0;
        mAttrSet = attrs;
        HiLog.info(label, "init");
        if (attrs.getAttr(BLComponentConstant.BL_STROKE_WIDTH).isPresent()) {
            mStrokeWidth = attrs.getAttr(BLComponentConstant.BL_STROKE_WIDTH).get().getDimensionValue();
        }
        if (attrs.getAttr(BLComponentConstant.BL_STROKE_COLOR).isPresent()) {
            mStrokeColor = attrs.getAttr(BLComponentConstant.BL_STROKE_COLOR).get().getColorValue();
        }

        if (attrs.getAttr(BLComponentConstant.BL_STROKE_POSITION).isPresent()) {
            mStrokePosition = attrs.getAttr(BLComponentConstant.BL_STROKE_POSITION).get().getStringValue();
        }

        if (attrs.getAttr(BLComponentConstant.BL_DRAWABLE_POSITION).isPresent()) {
            mMapElementPosition = attrs.getAttr(BLComponentConstant.BL_DRAWABLE_POSITION).get().getStringValue();
        }
        mPressMapElement = BLDrawableTool.getPixelElement(mAttrSet, BLComponentConstant.BL_PRESSED_DRAWABLE);
        mUnPressMapElement = BLDrawableTool.getPixelElement(mAttrSet, BLComponentConstant.BL_UNPRESSED_DRAWABLE);
        mCheckedElement = BLDrawableTool.getPixelElement(attrs, BLComponentConstant.BL_CHECKED_DRAWABLE);
        mUnCheckedElement = BLDrawableTool.getPixelElement(attrs, BLComponentConstant.BL_UNCHECKED_DRAWABLE);
        mSelectedElement = BLDrawableTool.getPixelElement(attrs, BLComponentConstant.BL_SELECTED_DRAWABLE);
        mUnselectedElement = BLDrawableTool.getPixelElement(attrs, BLComponentConstant.BL_UNSELECTED_DRAWABLE);
        mEnabledMapElement = BLDrawableTool.getPixelElement(attrs, BLComponentConstant.BL_ENABLED_DRAWABLE);
        mUnEnabledMapElement = BLDrawableTool.getPixelElement(attrs, BLComponentConstant.BL_UNENABLED_DRAWABLE);

        mSelectedTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_SELECTED_TEXT_COLOR);
        mUnSelectedTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_UNSELECTED_TEXT_COLOR);
        mPressedTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_PRESS_TEXT_COLOR);
        mUnpressedTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_UNPRESS_TEXT_COLOR);
        mCheckedTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_CHECKED_TEXT_COLOR);
        mUnCheckedTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_UNCHECKED_TEXT_COLOR);
        mEnabledTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_ENABLED_TEXT_COLOR);
        mUnEnabledTextColor = BLDrawableTool.getColor(attrs, BLComponentConstant.BL_UNENABLED_TEXT_COLOR);

        handleDraw();
    }


    public void create(Component component, AttrSet attrSet) {
        this.mComponent = component;
        init(attrSet);
    }

    private void handleDraw() {
        // 初始化画笔
        initPaint();
        if (TextTool.isNullOrEmpty(mStrokePosition)) {
            mComponent.setBackground(DrawableFactory.getDrawable(mAttrSet));
        }
       StateElement stateElement = DrawableFactory.getButtonDrawable(mAttrSet);
        if (stateElement!=null && stateElement.getStateCount()> 0) {
            mComponent.setBackground(DrawableFactory.getButtonDrawable(mAttrSet));
        }

        HiLog.info(label, "ButtonDrawableCreator stateCount:%{public}d", DrawableFactory.getButtonDrawable(mAttrSet).getStateCount());
        if (getStateListDrawable()!=null && getStateListDrawable().getStateCount()>0){
            handleBackgroundElement();
        }

        handleTextColor();

        if (DrawableFactory.getAnimationDrawable(mAttrSet).getNumberOfFrames()>0){
            mComponent.setBackground(DrawableFactory.getAnimationDrawable(mAttrSet));
        }
        mComponent.setTouchEventListener(null);
        mComponent.setTouchEventListener(this::onTouchEvent);
    }

    private void initPaint() {
        HiLog.info(label, "initPaint");
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(mStrokeColor == null ? Color.WHITE : mStrokeColor);
        mPaint.setStrokeWidth(mStrokeWidth);

        mComponent.addDrawTask(this::onDraw);

    }

    private void handleTextColor() {
        //处理文字颜色
        if (mComponent instanceof Checkbox) {
            Checkbox checkbox = (Checkbox) mComponent;
            if (mCheckedTextColor != null && mUnCheckedTextColor != null) {
                checkbox.setTextColor(checkbox.isChecked() ? mCheckedTextColor : mUnCheckedTextColor);
                checkbox.setCheckedStateChangedListener(((absButton, b) -> checkbox.setTextColor(b ? mCheckedTextColor : mUnCheckedTextColor)));
            }
        } else if (mComponent instanceof RadioButton) {
            RadioButton radioButton = (RadioButton) mComponent;
            if (mCheckedTextColor != null && mUnCheckedTextColor != null) {
                radioButton.setTextColor(radioButton.isChecked() ? mCheckedTextColor : mUnCheckedTextColor);
                radioButton.setCheckedStateChangedListener(((absButton, b) -> radioButton.setTextColor(b ? mCheckedTextColor : mUnCheckedTextColor)));
            }
        } else if (mComponent instanceof Text) {
            Text text = (Text) mComponent;
            if (mPressedTextColor != null && mUnpressedTextColor != null) {
                text.setTextColor(text.isPressed() ? mPressedTextColor : mUnpressedTextColor);
            } else if (mSelectedTextColor != null && mUnSelectedTextColor != null) {
                text.setTextColor(text.isSelected() ? mSelectedTextColor : mUnSelectedTextColor);
            } else if (mEnabledTextColor != null && mUnEnabledTextColor != null) {
                text.setTextColor(text.isEnabled() ? mEnabledTextColor : mUnEnabledTextColor);
            }
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int drawWidth = component.getWidth();
        int drawHeight = component.getHeight();
        HiLog.info(label, "width:%{public}d,height:%{public}d", drawWidth, drawHeight);
        canvas.save();
        if (!TextTool.isNullOrEmpty(mStrokePosition)) {
            drawLineOfPosition(canvas, drawHeight, drawWidth);
        }
        canvas.restore();
    }

    private void handleBackgroundElement() {
        if (mComponent == null) {
            return;
        }
        Text text =null;
        if (mComponent instanceof Text){
            text = (Text) mComponent;
        }
        if (text == null){
            return;
        }
        if (BLComponentConstant.BL_LEFT.equals(mMapElementPosition)) {
            mAroundPadding = BLDrawableTool.getDimension(mAttrSet, BLComponentConstant.BL_PADDING_LEFT,0);
            text.setAroundElements(getStateListDrawable(), null, null, null);
        } else if (BLComponentConstant.BL_TOP.equals(mMapElementPosition)) {
            mAroundPadding = BLDrawableTool.getDimension(mAttrSet, BLComponentConstant.BL_PADDING_TOP,0);
            text.setAroundElements(null, getStateListDrawable(), null, null);
        } else if (BLComponentConstant.BL_RIGHT.equals(mMapElementPosition)) {
            mAroundPadding = BLDrawableTool.getDimension(mAttrSet, BLComponentConstant.BL_PADDING_RIGHT,0);
            text.setAroundElements(null, null, getStateListDrawable(), null);
        } else if (BLComponentConstant.BL_BOTTOM.equals(mMapElementPosition)) {
            mAroundPadding = BLDrawableTool.getDimension(mAttrSet, BLComponentConstant.BL_PADDING_BOTTOM,0);
            text.setAroundElements(null, null, null, getStateListDrawable());
        } else {
            HiLog.info(label,"setMapElementDirection");
            text.setBackground(getStateListDrawable());
        }

    }

    private void drawLineOfPosition(Canvas canvas, int drawHeight, int drawWidth) {
        String[] positions = mStrokePosition.split("\\|");
        for (String position : positions) {
            if (BLComponentConstant.BL_LEFT.equals(position)) {
                canvas.drawLine(new Point(0, drawHeight), new Point(0, 0), mPaint);
            } else if (BLComponentConstant.BL_TOP.equals(position)) {
                canvas.drawLine(new Point(0, 0), new Point(drawWidth, 0), mPaint);
            } else if (BLComponentConstant.BL_RIGHT.equals(position)) {
                canvas.drawLine(new Point(drawWidth, 0), new Point(drawWidth, drawHeight), mPaint);
            } else if (BLComponentConstant.BL_BOTTOM.equals(position)) {
                canvas.drawLine(new Point(drawWidth, drawHeight), new Point(0, drawHeight), mPaint);
            } else {
                throw new LayoutScatterException("xml stroke position configure is error");
            }
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        HiLog.info(label, "onTouchEvent action:%{public}d", touchEvent.getAction());
        if (mComponent instanceof Text) {
            if (mPressedTextColor !=null && mUnpressedTextColor!=null){
                if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                    ((Text) mComponent).setTextColor(mPressedTextColor);
                    mComponent.setPressState(true);
                } else {
                    ((Text) mComponent).setTextColor(mUnpressedTextColor);
                    mComponent.setPressState(false);
                }
//                handleTextColor();
            }
            if (mSelectedTextColor !=null && mUnSelectedTextColor!=null){
                if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                    mClickCount++;
                    //点击两次 取消选中
                    if (mClickCount % 2 == 0) {
                        ((Text) mComponent).setTextColor(mUnSelectedTextColor);
                        mComponent.setSelected(false);
                    } else {
                        ((Text) mComponent).setTextColor(mSelectedTextColor);
                        mComponent.setSelected(true);
                    }
                }
//                handleTextColor();
            }
        }

        return true;
    }

    private StateElement getStateListDrawable() {
        HiLog.info(label,"getStateListDrawable");
        StateElement stateListDrawable = new StateElement();

        if (mCheckedElement != null) {
            stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, mCheckedElement);
        }
        if (mUnCheckedElement != null) {
            stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_CHECKED}, mUnCheckedElement);
        }
        if (mEnabledMapElement != null) {
            stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, mEnabledMapElement);
        }
        if (mUnEnabledMapElement != null) {
            stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_DISABLED}, mUnEnabledMapElement);
        }
        if (mSelectedElement != null) {
            stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, mSelectedElement);
        }
        if (mUnselectedElement != null) {
            stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_SELECTED}, mUnselectedElement);
        }
        if (mPressMapElement != null) {
            stateListDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, mPressMapElement);
        }
        if (mUnPressMapElement != null) {
            stateListDrawable.addState(new int[]{-ComponentState.COMPONENT_STATE_PRESSED}, mUnPressMapElement);
        }
        HiLog.info(label,"stateListDrawable count:%{public}d",stateListDrawable.getStateCount());
        return stateListDrawable;
    }
}
