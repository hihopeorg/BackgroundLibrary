package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ScrollView;
import ohos.app.Context;

public class BLScrollView extends ScrollView {
    public BLScrollView(Context context) {
        super(context);
    }

    public BLScrollView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public BLScrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
