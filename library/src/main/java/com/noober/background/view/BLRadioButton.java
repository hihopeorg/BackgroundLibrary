package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.RadioButton;
import ohos.app.Context;

public class BLRadioButton  extends RadioButton {
    public BLRadioButton(Context context) {
        super(context);
    }

    public BLRadioButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLRadioButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        setButtonElement(null);
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
