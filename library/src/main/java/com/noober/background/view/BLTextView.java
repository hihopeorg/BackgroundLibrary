package com.noober.background.view;


import ohos.agp.components.*;
import ohos.app.Context;



public class BLTextView extends Text{
    public BLTextView(Context context) {
        super(context);
    }

    public BLTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BLTextView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);

    }
}
