package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;

import ohos.app.Context;


public class BLView extends Component {

    public BLView(Context context) {
        super(context);
    }

    public BLView(Context context, AttrSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public BLView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttrSet attrs) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrs);
    }


}
