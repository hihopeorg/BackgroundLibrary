package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Checkbox;
import ohos.app.Context;

public class BLCheckBox extends Checkbox {
    public BLCheckBox(Context context) {
        super(context);
    }

    public BLCheckBox(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLCheckBox(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
