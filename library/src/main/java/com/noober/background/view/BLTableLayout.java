package com.noober.background.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.TableLayout;
import ohos.app.Context;

public class BLTableLayout extends TableLayout {
    public BLTableLayout(Context context) {
        super(context);
    }

    public BLTableLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public BLTableLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }
    private void init(AttrSet attrSet) {
        new BLComponentDrawTask().setBackgroundDrawable(this,attrSet);
    }
}
