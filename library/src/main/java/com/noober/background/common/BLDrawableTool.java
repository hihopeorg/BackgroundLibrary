/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noober.background.common;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;

import java.util.Optional;

public class BLDrawableTool {

    public static Color getColor(AttrSet attrSet,String attrKey) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getColorValue();
        }
        return null;
    }

    public static Element getPixelElement(AttrSet attrSet, String attrKey) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getElement();
        }
        return null;
    }

    public static int  getInt(AttrSet attrSet,String attrKey){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getIntegerValue();
        }
        return 0;
    }

    public static int  getDimension(AttrSet attrSet,String attrKey,int defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getDimensionValue();
        }
        return defaultValue;
    }

    public static int getColor(AttrSet attrSet, String attrKey,Color colorValue) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getColorValue().getValue();
        }
        return colorValue.getValue();
    }

    public static int  getInt(AttrSet attrSet,String attrKey,int defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getIntegerValue();
        }
        return defaultValue;
    }

    public static float  getFloat(AttrSet attrSet,String attrKey,float defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getFloatValue();
        }
        return defaultValue;
    }

    public static boolean  getBoolean(AttrSet attrSet,String attrKey,boolean defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getBoolValue();
        }
        return defaultValue;
    }

    public static String  getString(AttrSet attrSet,String attrKey,String defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getStringValue();
        }
        return defaultValue;
    }
}
