# BackgroundLibrary

**本项目是基于开源项目BackgroundLibrary进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/JavaNoober/BackgroundLibrary ）追踪到原项目版本**

#### 项目介绍

- 项目名称：BackgroundLibrary
- 所属系列：ohos的第三方组件适配移植
- 功能：用户开发过程中，不需要自定义View，直接添加标签即可自动生成shape和selector效果
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/JavaNoober/BackgroundLibrary
- 原项目基线版本：1.6.5 sha1:b8269931967945ec97a720b98f9d304f4c1859d2
- 编程语言：Java
- 外部库依赖：无

#### 效果
  <img src="screenshot/operation.gif"/>  

#### 安装教程

方法一：

1. 编译依赖库har包BackgroundLibrary.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.noober.background.ohos:backgroundlibrary:1.0.0'
}
```

#### 使用说明

1. 边框+背景+圆角
```xml
<com.noober.background.view.BLButton
       ohos:id="$+id:btnSkip"
       ohos:height="70vp"
       ohos:width="match_parent"
       ohos:text="跳转到列表"
       ohos:text_alignment="center"
       ohos:text_size="18fp"
       ohos:top_margin="10vp"
       app:bl_corners_radius="20vp"
       app:bl_shape="rectangle"
       app:bl_solid_color="#FF46E41A"
       app:bl_stroke_color="#FFE11919"
       app:bl_stroke_width="2vp"/>      
```
等同于
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:ohos="http://schemas.huawei.com/res/ohos"
       ohos:shape="rectangle">
    <corners ohos:radius="20vp" />
    <stroke ohos:width="2vp"
            ohos:color="#FFE11919"/>
    <solid ohos:color="#FF46E41A" />
</shape>
```
2.渐变
```xml
<com.noober.background.view.BLTextView
            ohos:id="$+id:tvConfirm"
            ohos:height="70vp"
            ohos:width="match_parent"
            ohos:text="确认"
            ohos:text_alignment="center"
            ohos:text_size="18fp"
            ohos:top_margin="10vp"
            app:bl_corners_radius="20vp"
            app:bl_gradient_angle="0"
            app:bl_gradient_centerColor="#CD4F39"
            app:bl_gradient_endColor="#CD2626"
            app:bl_gradient_startColor="#CD5B45"
            app:bl_shape="rectangle"/>         
```
等同于
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:ohos="http://schemas.huawei.com/res/ohos"
       ohos:shape="rectangle">
    <corners ohos:radius="20vp" />
    <solid ohos:color="#CD4F39,#CD2626,#CD5B45" />
</shape>
```

2.点赞效果
```xml
        <com.noober.background.view.BLButton
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text_size="18fp"
            ohos:top_margin="10vp"
            app:bl_position="left"
            app:bl_pressed_drawable="$media:circle_like_pressed"
            app:bl_unPressed_drawable="$media:circle_like_normal"/>        
```
等同于
```xml
<?xml version="1.0" encoding="utf-8"?>
<state-container xmlns:ohos="http://schemas.huawei.com/res/ohos">
    <item ohos:state="component_state_pressed" ohos:element="$media:circle_like_pressed"/>
    <item ohos:state="component_state_empty" ohos:element="$media:circle_like_normal"/>
</state-container>
```
通过代码设置
```java
ShapeElement  pressedShape = (ShapeElement) ElementScatter.getInstance(this).parse(ResourceTable.Graphic_shape_gender_green);
ShapeElement unPressedShape = (ShapeElement) ElementScatter.getInstance(this).parse(ResourceTable.Graphic_shape_gender_red);
Element draable1= new DrawableCreator.Builder()
        .setPressedDrawable(pressedShape)
        .setUnPressedDrawable(unPressedShape)
        .build();
```
3.点击文字变色及边框变色
```xml
        <com.noober.background.view.BLButton
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:padding="4vp"
            ohos:text="点击边框变色"
            ohos:text_size="18fp"
            ohos:top_margin="10vp"
            app:bl_pressed_solid_color="#FFDEAD"
            app:bl_pressed_stroke_color="#C6E2FF"
            app:bl_pressed_textColor="#98FB98"
            app:bl_stroke_width="3vp"
            app:bl_unPressed_solid_color="#E9967A"
            app:bl_unPressed_stroke_color="#98FB98"
            app:bl_unPressed_textColor="#FF0E0F0E"/>
```
4.设置drawableLeft
```xml
        <com.noober.background.view.BLButton
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:text="未点赞"
            ohos:text_size="18fp"
            ohos:top_margin="10vp"
            app:bl_padding_left="10vp"
            app:bl_position="left"
            app:bl_selected_drawable="$media:circle_like_pressed"
            app:bl_selected_textColor="#FFCE9B32"
            app:bl_unSelected_drawable="$media:circle_like_normal"
            app:bl_unSelected_textColor="#FF0E0F0E"/>
```
5.设置帧动画
```xml
   <com.noober.background.view.BLView
            ohos:height="300vp"
            ohos:width="match_content"
            ohos:margin="20vp"
            app:bl_anim_auto_start="true"
            app:bl_duration="50"
            app:bl_frame_drawable_item0="$media:img00"
            app:bl_frame_drawable_item1="$media:img01"
            app:bl_frame_drawable_item10="$media:img10"
            app:bl_frame_drawable_item11="$media:img11"
            app:bl_frame_drawable_item12="$media:img12"
            app:bl_frame_drawable_item13="$media:img13"
            app:bl_frame_drawable_item14="$media:img14"
            app:bl_frame_drawable_item2="$media:img02"
            app:bl_frame_drawable_item3="$media:img03"
            app:bl_frame_drawable_item4="$media:img04"
            app:bl_frame_drawable_item5="$media:img05"
            app:bl_frame_drawable_item6="$media:img06"
            app:bl_frame_drawable_item7="$media:img07"
            app:bl_frame_drawable_item8="$media:img08"
            app:bl_frame_drawable_item9="$media:img09"
            app:bl_oneshot="false"/>
```

#### 版本迭代


- v1.0.0


#### 版权和许可信息
```
Apache License ，version 2.0
```

