package com.noober.backgroundlibrary.provider;

import com.noober.background.ResourceTable;
import com.noober.backgroundlibrary.bean.SampleItem;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;

import java.util.List;


public class SampleItemProvider extends BaseItemProvider {
    private List<SampleItem> list;
    private Ability slice;
    public SampleItemProvider(List<SampleItem> list, Ability slice) {
        this.list = list;
        this.slice = slice;
    }
    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }
    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()){
            return list.get(position);
        }
        return null;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item, null, false);
        } else {
            cpt = convertComponent;
        }
        return cpt;
    }
}