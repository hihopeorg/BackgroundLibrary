package com.noober.backgroundlibrary;

import com.noober.background.ResourceTable;
import com.noober.background.view.BLListView;
import com.noober.backgroundlibrary.bean.SampleItem;
import com.noober.backgroundlibrary.provider.SampleItemProvider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

public class ListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ablility_grid);
        initListContainer();

    }

    private void initListContainer() {
        BLListView listContainer = (BLListView) findComponentById(ResourceTable.Id_grid_container);
        List<SampleItem> list = getData();
        SampleItemProvider sampleItemProvider = new SampleItemProvider(list, this);
        listContainer.setItemProvider(sampleItemProvider);
        listContainer.setItemClickedListener((container, component, position, id) -> {
            SampleItem item = (SampleItem) listContainer.getItemProvider().getItem(position);
        });
    }
    private ArrayList<SampleItem> getData() {
        ArrayList<SampleItem> list = new ArrayList<>();
        for (int i = 0; i <= 30; i++) {
            list.add(new SampleItem("Item" + i));
        }
        return list;
    }
}
