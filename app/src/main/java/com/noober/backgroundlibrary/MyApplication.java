package com.noober.backgroundlibrary;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }
}
