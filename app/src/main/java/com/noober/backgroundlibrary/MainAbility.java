package com.noober.backgroundlibrary;

import com.noober.background.ResourceTable;
import com.noober.background.drawable.DrawableCreator;
import com.noober.background.view.BLButton;
import com.noober.background.view.BLTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import java.io.File;

public class MainAbility extends Ability {
    private BLButton btnLike;
    private int clickNum = 1;
    private int clickNumT = 1;
    private int clickTvState = 1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        BLButton button = (BLButton) findComponentById(ResourceTable.Id_btnSkip);
        BLButton btnLike = (BLButton) findComponentById(ResourceTable.Id_bl_like1);
        BLButton btnLike2 = (BLButton) findComponentById(ResourceTable.Id_bl_like2);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Operation operation = new Intent.OperationBuilder()
                        .withAction("com.noober.backgroundlibrary.ListAbility")
                        .withAbilityName(ListAbility.class.getSimpleName())
                        .build();
                Intent intent1 = new Intent();
                intent1.setOperation(operation);

                startAbility(intent1);
            }
        });

        btnLike.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                clickNum ++;
                if (clickNum%2==0){
                   btnLike.setText("已点赞");
                }else {
                    btnLike.setText("未点赞");
                }
            }
        });
        btnLike2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                clickNumT++;
                btnLike2.setText("点赞+"+clickNumT);
            }
        });
        BLTextView blTextView = (BLTextView) findComponentById(ResourceTable.Id_tv_state);
        BLTextView stateDesc = (BLTextView) findComponentById(ResourceTable.Id_tv_state_desc);
        blTextView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (stateDesc.isEnabled()){
                    stateDesc.setEnabled(false);
                    stateDesc.setTextColor(new Color(Color.getIntColor("#EE0000")));
                    stateDesc.setText("textView一条属性多个状态：enable=false");
                }else {
                    stateDesc.setTextColor(new Color(Color.getIntColor("#FFE360CF")));
                    stateDesc.setEnabled(true);
                    stateDesc.setText("textView一条属性多个状态：enable=true");
                }

            }
        });
        Text tv2 = (Text) findComponentById(ResourceTable.Id_creator_tv2);

        DrawableCreator.Builder  builder = new DrawableCreator.Builder()
                .setPressedTextColor(getColor(ResourceTable.Color_pressed_text_color))
                .setUnPressedTextColor(getColor(ResourceTable.Color_unpressed_text_color))
                .buildTextColor(tv2);

    }
}
