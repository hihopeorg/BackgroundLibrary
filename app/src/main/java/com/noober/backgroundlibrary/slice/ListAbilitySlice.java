package com.noober.backgroundlibrary.slice;

import com.noober.background.ResourceTable;
import com.noober.background.view.BLGridView;
import com.noober.background.view.BLListView;
import com.noober.backgroundlibrary.bean.SampleItem;
import com.noober.backgroundlibrary.provider.SampleItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

public class ListAbilitySlice extends AbilitySlice {

    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000302, "ListAbilitySlice");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ablility_grid);
        initListContainer();
    }


    private void initListContainer() {
        BLListView listContainer = (BLListView) findComponentById(ResourceTable.Id_grid_container);
        List<SampleItem> list = getData();
        SampleItemProvider sampleItemProvider = new SampleItemProvider(list, getAbility());
        listContainer.setItemProvider(sampleItemProvider);
        listContainer.setItemClickedListener((container, component, position, id) -> {
            SampleItem item = (SampleItem) listContainer.getItemProvider().getItem(position);
        });
    }
    private ArrayList<SampleItem> getData() {
        ArrayList<SampleItem> list = new ArrayList<>();
        for (int i = 0; i <= 30; i++) {
            list.add(new SampleItem("Item" + i));
        }
        return list;
    }
}
