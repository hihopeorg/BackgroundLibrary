package com.noober.backgroundlibrary.slice;

import com.noober.background.ResourceTable;
import com.noober.background.drawable.DrawableCreator;
import com.noober.background.view.BLTextView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.*;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


import java.io.IOException;


public class ApiSlice  extends AbilitySlice {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000308, "ApiSlice");
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_api);
        build();
    }


        private void build(){
            Element drawable = new DrawableCreator.Builder().setCornersRadius(30)
                    .setSolidColor(getColor(ResourceTable.Color_pressed_solid_color))
                    .setStrokeColor(getColor(ResourceTable.Color_stroke_color))
                    .setStrokeWidth(9)
                    .setShape(DrawableCreator.Shape.Rectangle)
                    .build();
            BLTextView tv1= (BLTextView) findComponentById(ResourceTable.Id_creator_tv1);
            tv1.setBackground(drawable);

            Text tv2 = (Text) findComponentById(ResourceTable.Id_creator_tv2);

            DrawableCreator.Builder  builder = new DrawableCreator.Builder()
                    .setPressedTextColor(getColor(ResourceTable.Color_pressed_text_color))
                    .setUnPressedTextColor(getColor(ResourceTable.Color_unpressed_text_color))
                    .buildTextColor(tv2);

            ShapeElement element = new ShapeElement();
            element.setShape(ShapeElement.RECTANGLE);
            element.setRgbColor(RgbColor.fromArgbInt(new Color(getColor(ResourceTable.Color_unPressed_solid_color)).getValue()));
            tv2.setBackground(element);

            ShapeElement  pressedShape = (ShapeElement) ElementScatter.getInstance(this).parse(ResourceTable.Graphic_shape_gender_green);
            ShapeElement unPressedShape = (ShapeElement) ElementScatter.getInstance(this).parse(ResourceTable.Graphic_shape_gender_red);
            Element draable1= new DrawableCreator.Builder()
                    .setPressedDrawable(pressedShape)
                    .setUnPressedDrawable(unPressedShape)
                    .build();

            Button  button1 = (Button) findComponentById(ResourceTable.Id_creator_btn1);
            button1.setBackground(draable1);




            Element  drawable2 = new DrawableCreator.Builder()
                    .setPressedStrokeColor(getColor(ResourceTable.Color_pressed_solid_color),
                            getColor(ResourceTable.Color_unPressed_solid_color))
                    .setPressedSolidColor(getColor(ResourceTable.Color_unPressed_solid_color),getColor(ResourceTable.Color_pressed_solid_color))
                    .setStrokeWidth(9)
                    .build();

            Button button = (Button) findComponentById(ResourceTable.Id_creator_btn2);
            button.setBackground(drawable2);

            Text  text = (Text) findComponentById(ResourceTable.Id_creator_tv3);
            try {
                Resource  resource = getResourceManager().getResource(ResourceTable.Media_circle_like_normal);
                PixelMapElement pixelMapElement = new PixelMapElement(resource);
                DrawableCreator.setDrawable(pixelMapElement,text, DrawableCreator.DrawablePosition.Left);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }


            Element drawable3= new DrawableCreator.Builder()
                    .setCheckedDrawable(pressedShape)
                    .setUnCheckedDrawable(unPressedShape)
                    .build();

            Element drawable4= new DrawableCreator.Builder()
                    .setCheckedDrawable(pressedShape)
                    .setUnCheckedDrawable(unPressedShape)
                    .build();
            RadioButton radio1 = (RadioButton) findComponentById(ResourceTable.Id_rb1);
            RadioButton radio2 = (RadioButton) findComponentById(ResourceTable.Id_rb2);
            radio1.setButtonElement(null);
            radio2.setButtonElement(null);


            radio1.setBackground(drawable3);
            radio2.setBackground(drawable4);

            RadioContainer radioContainer = (RadioContainer) findComponentById(ResourceTable.Id_rc_one);
            radioContainer.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
                @Override
                public void onCheckedChanged(RadioContainer radioContainer, int i) {
                    HiLog.info(label,"radioContainer:%{public}d",i);
                    if (i==0){
                        radio2.setChecked(false);
                        radio1.setChecked(true);
                    }else {
                        radio1.setChecked(false);
                        radio2.setChecked(true);
                    }
                }
            });



            Button  button2 = (Button) findComponentById(ResourceTable.Id_btn4);
            try {
                Resource  resource = getResourceManager().getResource(ResourceTable.Media_circle_like_normal);
                Resource  resource2 = getResourceManager().getResource(ResourceTable.Media_circle_like_pressed);
                Element pixelMapElement = new PixelMapElement(resource);
                Element pixelMapElement2 = new PixelMapElement(resource2);
                Element element1 = new DrawableCreator.Builder().setPressedDrawable(pixelMapElement2)
                        .setUnPressedDrawable(pixelMapElement).build();
                button2.setBackground(element1);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }


            Button  button3 = (Button) findComponentById(ResourceTable.Id_btn5);
            try {
                Resource  resource = getResourceManager().getResource(ResourceTable.Media_circle_like_normal);
                Resource  resource2 = getResourceManager().getResource(ResourceTable.Media_circle_like_pressed);
                Element pixelMapElement = new PixelMapElement(resource);
                Element pixelMapElement2 = new PixelMapElement(resource2);
                Element element2 = new DrawableCreator.Builder().setSelectedDrawable(pixelMapElement2)
                        .setUnSelectedDrawable(pixelMapElement).build();
                button3.setBackground(element2);
                button3.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if(button3.isSelected()){
                            button3.setText("被选中了");
                        }else {
                            button3.setText("已选中了");
                        }
                        button3.setSelected(!button3.isSelected());
                    }
                });


            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            Element drawable6 = new DrawableCreator.Builder().setCornersRadius(30)
                    .setGradientAngle(0)
                    .setGradient(DrawableCreator.Gradient.Linear)
                    .setGradientColor(getColor(ResourceTable.Color_pressed_solid_color),getColor(ResourceTable.Color_unPressed_solid_color),getColor(ResourceTable.Color_stroke_color))
                    .setShape(DrawableCreator.Shape.Rectangle)
                    .build();
            Text  text6= (Text) findComponentById(ResourceTable.Id_tv6);
            text6.setBackground(drawable6);


            // around textColor  变化
            Text  text7 = (Text) findComponentById(ResourceTable.Id_creator_tv4);
            DrawableCreator.Builder  builder2 = new DrawableCreator.Builder()
                    .setPressedTextColor(getColor(ResourceTable.Color_pressed_text_color))
                    .setUnPressedTextColor(getColor(ResourceTable.Color_unpressed_text_color))
                    .buildTextColor(text7);

            try {
                Resource  resource = getResourceManager().getResource(ResourceTable.Media_circle_like_normal);
                PixelMapElement pixelMapElement = new PixelMapElement(resource);
                DrawableCreator.setDrawable(pixelMapElement,text7, DrawableCreator.DrawablePosition.Left);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            PixelMapElement defaultPixelMapElement= null;
            try {
                Resource  resource = getResourceManager().getResource(ResourceTable.Media_circle_like_normal);
                defaultPixelMapElement = new PixelMapElement(resource);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            PixelMapElement updatePixelMapElement= null;
            try {
                Resource  resource = getResourceManager().getResource(ResourceTable.Media_circle_like_pressed);
                updatePixelMapElement = new PixelMapElement(resource);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            StateElement stateElement = new StateElement();
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED},updatePixelMapElement);
            stateElement.addState(new int[]{-ComponentState.COMPONENT_STATE_PRESSED},defaultPixelMapElement);
            DrawableCreator.setDrawable(stateElement,text7, DrawableCreator.DrawablePosition.Left);
        }



}
