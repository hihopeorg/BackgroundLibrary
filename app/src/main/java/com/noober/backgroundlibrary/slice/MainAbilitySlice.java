package com.noober.backgroundlibrary.slice;

import com.noober.background.ResourceTable;
import com.noober.background.view.BLButton;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


public class MainAbilitySlice extends AbilitySlice {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000300, "MainAbilitySlice");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        BLButton button = (BLButton) findComponentById(ResourceTable.Id_btnSkip);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new ListAbilitySlice(),new Intent());
            }
        });


    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
