package com.noober.backgroundlibrary.slice;


import com.noober.background.ResourceTable;
import com.noober.backgroundlibrary.User;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.abilityjet.databinding.DataBinding;
import ohos.aafwk.abilityjet.databinding.DataBindingUtil;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import ohos.agp.components.LayoutScatter;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

public class DataBindingSlice extends AbilitySlice {


    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000303, "DataBindingSlice");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setUIContent(ResourceTable.Layout_activity_data_binding);
        User  user = new User();
        user.setName("Skip to list");

//
        Component componentContainer = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_activity_data_binding, null, false);
//        try {
//            UserBinding binding = DataBindingUtil.createBinding(ResourceTable.Layout_activity_data_binding, this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        binding.initComponent(componentContainer);
//        binding.setLifecycle(getLifecycle());
//        binding.setFruit(fruit);
    }
}
