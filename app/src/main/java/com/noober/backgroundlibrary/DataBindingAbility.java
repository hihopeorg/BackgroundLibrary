package com.noober.backgroundlibrary;

import com.noober.backgroundlibrary.slice.DataBindingSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DataBindingAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DataBindingSlice.class.getName());
    }
}
