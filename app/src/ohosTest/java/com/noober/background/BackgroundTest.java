/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noober.background;

import com.noober.background.drawable.DrawableCreator;
import com.noober.background.view.BLButton;
import com.noober.background.view.BLTextView;
import com.noober.backgroundlibrary.MainAbility;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.colors.ColorConverter;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;



/**
 * 功能性测试
 */
public class BackgroundTest extends TestCase {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private MainAbility ability;
    private ShapeElement element;
    private DrawableCreator.Builder  builder;
    private Button blButton;


    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(ability, 5);
        blButton = new Button(ability);
        builder = new DrawableCreator.Builder().setCornersRadius(30)
                .setSolidColor(ability.getColor(ResourceTable.Color_pressed_solid_color))
                .setStrokeColor(ability.getColor(ResourceTable.Color_stroke_color))
                .setStrokeWidth(9)
                .setShape(DrawableCreator.Shape.Rectangle);
        ShapeElement drawable = (ShapeElement) builder.build();
        blButton.setBackground(drawable);
        element = (ShapeElement) blButton.getBackgroundElement();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
     EventHelper.clearAbilities();
    }

    @Test
    public void testBackground() {
        assertNotNull("testBackground failed, element is null", element);
    }

    @Test
    public void testStrokeWidth(){

        int strokeWidth = element.getStrokeWidth();
        System.out.println("songyu testStrokeWidth:"+strokeWidth);
        assertEquals("testStrokeWidth",9,strokeWidth);
    }

    @Test
    public void testCornerRadius (){
        float cornerRadius = element.getCornerRadius();
        assertEquals("testStrokeWidth",30.0f,cornerRadius);
    }

    @Test
    public void  testShape(){
        int shape = element.getShape();
        assertEquals("testStrokeWidth",ShapeElement.RECTANGLE,shape);
    }

    @Test
    public void  testStrokeColor(){
        RgbColor rgbColor = element.getStrokeColor();
        assertEquals("testStrokeColor",ability.getColor(ResourceTable.Color_stroke_color),rgbColor.asArgbInt());
    }

    @Test
    public void testSolidColor() {
        RgbColor rgbColor = element.getRgbColors()[0];
        assertEquals("testSolidColor",ability.getColor(ResourceTable.Color_pressed_solid_color),rgbColor.asArgbInt());
    }
}