/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noober.background;


import com.noober.background.view.BLButton;
import com.noober.backgroundlibrary.MainAbility;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicReference;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * UI自动化测试
 */

public class ExampleOhosTest {
    private static final String TAG = "ExampleOhosTest";

   private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
   private MainAbility ability;
   private IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

   @Before
   public void abtainAbility() {
       ability = EventHelper.startAbility(MainAbility.class);
       EventHelper.waitForActive(ability,5);
   }
    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    @Test
    public void testClickBackground() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BLButton  blButton = (BLButton) ability.findComponentById(ResourceTable.Id_btn_login);
        iAbilityDelegator.runOnUIThreadSync(()->{
            blButton.setPressState(true);

        });
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StateElement stateElement = (StateElement) blButton.getBackgroundElement();
        ShapeElement shapeElement = (ShapeElement) stateElement.getCurrentElement();
        assertNotNull(TAG,shapeElement);
        RgbColor rgbColor = shapeElement.getRgbColors()[0];
        System.out.println(TAG+","+rgbColor.asArgbInt());
        assertEquals(TAG, Color.getIntColor("#FF509124"),rgbColor.asArgbInt());
        iAbilityDelegator.runOnUIThreadSync(()->blButton.setPressState(false));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}